Piosition-independed Loader by Yattering
========================================

This is simple x86 loader. It downloads file from internet and try to execute it. By default in this code loader try to download FASM demo from [SEH VM Engine by Yattering](https://gitlab.com/yattering/SEH_based_VM/).
Some features of loader:
- Position independed code
- All data generate by code in stack
- Dont't use `GetProcAddress` WINAPI function.

## Files ##

- `compile.cmd`
- `README.md` This readme file.
- `settings.inc` Settings for loader compilation. It contains file URL (may be not only exe but any file), destination file path and output file format settings.
- `loader.asm` Loader source code file.
- `loader.exe` Win32 PE x86 executable version of loader.
- `loader.dll` Win32 PE x86 dynamic library version of loader.
- `loader.bin` Win32 x86 shellcode version of loader.
- `logo.gif`

## Contacts ##

If you have any question, you can find me by contacts below:
- e-mail: yattering (at) sigaint (d0t) org
- jabber: yattering (at) xmpp (d0t) jp

_to be continued..._

![Coded by Yattering](https://gitlab.com/yattering/Loader/raw/master/logo.gif)